﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoneyLoverTUBUI;
using MoneyLoverTUBUI.Controllers;
using System;
using System.Linq;
//using NUnit.Framework;
using System.Web.Mvc;






namespace TESTSTK
{
    [TestClass]
    public class STKUnitTest
    {
        private Entities db = new Entities();
        readonly string thongbaoSucess = "<script>document.getElementById('alertboxSucess').style.display = 'block'</script>";

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.About() as ViewResult;
            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.Contact() as ViewResult;
            // Assert
            Assert.AreEqual("Your contact page.", result.ViewBag.Message);
        }
        [TestMethod]
        public void TrangGioiThieu()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.TrangGioiThieu() as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void tattoan()
        {
            string tinhtrang = "Close";
            var TienLai = 0;
            if (tinhtrang == "Close")
            {
                DateTime date = new DateTime(2018, 11, 4);
                TimeSpan timeSpan = DateTime.Now - date;
                int demngay = timeSpan.Days;
                TienLai = (int)(365000 * 0.05 * demngay / 365);
            }
            Assert.AreEqual(550, TienLai);
        }
        
        [TestMethod] // id = null
        public void Tattoan_UTC01()
        {
            STKsController controller = new STKsController();
            ViewResult result = controller.Tattoaninput(null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("id rỗng", result.ViewBag.Message);
            Assert.AreEqual("<script>document.getElementById('alertboxFail').style.display = 'block'</script>", result.TempData["AlertFail"]);
            Assert.AreEqual("Index", result.ViewName);

        }
        [TestMethod]//id tồn tại
        public void Tattoan_UTC02()
        {
            var soTietKiem = db.STKs.ToList();
            foreach(var item in soTietKiem)
            {
                STKsController controller = new STKsController();
                ViewResult result = controller.Tattoaninput(item.IdSTK) as ViewResult;
                
                
                if (item.TinhTrang == "Open")
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual("Close", result.ViewBag.TinhTrang);
                    Assert.AreEqual("tất toán thành công , tiền lãi của bạn là :" + result.ViewBag.TienLai, result.ViewBag.Message);
                    Assert.AreEqual((int)(item.SoTienGoc * item.LaiSuatKhongKyHan * (DateTime.Now - (DateTime)item.NgayGui).TotalDays / 365), result.ViewBag.TienLai);
                    Assert.AreEqual("Index", result.ViewName);
                    Assert.AreEqual(thongbaoSucess, result.TempData["AlertSuccess"]);
                }
                if (item.TinhTrang == "Close")
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual("Close", result.ViewBag.TinhTrang);
                    Assert.AreEqual("Thao tác thất bại, bạn không thể tất toán sổ đã đóng", result.ViewBag.Message);
                    Assert.AreEqual("Index", result.ViewName);
                    Assert.AreEqual("<script>document.getElementById('alertboxFail').style.display = 'block'</script>", result.TempData["AlertFail"]);                }

            }
        }
        [TestMethod]//id k tồn tại
        public void Tattoan_UTC03()
        {
            var soTietKiem = db.STKs.ToList();
            int idkhongtontai = 1;
            foreach(var item in soTietKiem)
            {
                if (idkhongtontai == item.IdSTK) 
                { idkhongtontai++; }
            }
            STKsController controller = new STKsController();
            ViewResult result = controller.Tattoaninput(idkhongtontai) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("id không tồn tại", result.ViewBag.Message);
            Assert.AreEqual("<script>document.getElementById('alertboxFail').style.display = 'block'</script>", result.TempData["AlertFail"]);
            Assert.AreEqual("Index", result.ViewName);
        }

    }
}
