﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoneyLoverTUBUI;

namespace MoneyLoverTUBUI.Controllers
{
    public class NganHangsController : Controller
    {
        private Entities db = new Entities();

        // GET: NganHangs
        public ActionResult Index()
        {
            var nganHangs = db.NganHangs.Include(n => n.AspNetUser);
            return View(nganHangs.ToList());
        }

        // GET: NganHangs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NganHang nganHang = db.NganHangs.Find(id);
            if (nganHang == null)
            {
                return HttpNotFound();
            }
            return View(nganHang);
        }

        // GET: NganHangs/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: NganHangs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NganHang1,UserID")] NganHang nganHang)
        {
            if (ModelState.IsValid)
            {
                db.NganHangs.Add(nganHang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", nganHang.UserID);
            return View(nganHang);
        }

        // GET: NganHangs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NganHang nganHang = db.NganHangs.Find(id);
            if (nganHang == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", nganHang.UserID);
            return View(nganHang);
        }

        // POST: NganHangs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NganHang1,UserID")] NganHang nganHang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nganHang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", nganHang.UserID);
            return View(nganHang);
        }

        // GET: NganHangs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NganHang nganHang = db.NganHangs.Find(id);
            if (nganHang == null)
            {
                return HttpNotFound();
            }
            return View(nganHang);
        }

        // POST: NganHangs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            NganHang nganHang = db.NganHangs.Find(id);
            db.NganHangs.Remove(nganHang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
