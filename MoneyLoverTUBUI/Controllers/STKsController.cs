﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoneyLoverTUBUI;

namespace MoneyLoverTUBUI.Controllers
{
    public class STKsController : Controller
    {
        private Entities db = new Entities();

        // GET: STKs
        public ActionResult Index()
        {
            var sTKs = db.STKs.Include(s => s.AspNetUser);
            return View(sTKs.ToList());
        }

        // GET: STKs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STK sTK = db.STKs.Find(id);
            if (sTK == null)
            {
                return HttpNotFound();
            }
            return View(sTK);
        }

        // GET: STKs/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: STKs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdSTK,UserID,NganHang,NgayGui,SoTienGoc,KyHanGui,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] STK sTK)
        {
            if (ModelState.IsValid)
            {
                db.STKs.Add(sTK);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }

        // GET: STKs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STK sTK = db.STKs.Find(id);
            if (sTK == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }

        // POST: STKs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdSTK,UserID,NganHang,NgayGui,SoTienGoc,KyHanGui,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] STK sTK)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sTK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }

        // GET: STKs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STK sTK = db.STKs.Find(id);
            if (sTK == null)
            {
                return HttpNotFound();
            }
            return View(sTK);
        }

        // POST: STKs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            STK sTK = db.STKs.Find(id);
            db.STKs.Remove(sTK);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult DangNhap()
        {
            return View();
        }
        public ActionResult DangKy()
        {
            return View();
        }
        public ActionResult Tattoan(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "thao tác thất bại , id không thể trống ";
                TempData["AlertFail"] = "<script>document.getElementById('alertboxFail').style.display = 'block'</script>";
                return View("Index");
            }
            STK sTK = db.STKs.Find(id);
            if (sTK.TinhTrang == "Close")
            {
                ViewBag.Message = "sổ đã đóng , không thể tất toán nữa";
                TempData["AlertFail"] = "<script>document.getElementById('alertboxFail').style.display = 'block'</script>";
                return View("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            ViewBag.idstk = id;
            return View(sTK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Tattoaninput(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "id rỗng";
                TempData["AlertFail"] = "<script>document.getElementById('alertboxFail').style.display = 'block'</script>";
                return View("Index");
            }
            STK sTK = db.STKs.Find(id);
            bool idtontai = false;
            foreach(var item in db.STKs)
            {
                if (id == item.IdSTK) { idtontai = true; }
            }
            if (idtontai && sTK.TinhTrang =="Open")
            {
                sTK.TinhTrang = "Close";
                sTK.TienLai = (int)(sTK.SoTienGoc * sTK.LaiSuatKhongKyHan * (DateTime.Now - (DateTime)sTK.NgayGui).TotalDays / 365) ;
                db.Entry(sTK).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.TinhTrang = sTK.TinhTrang;
                ViewBag.TienLai = sTK.TienLai;
                ViewBag.Message = "tất toán thành công , tiền lãi của bạn là :"+ sTK.TienLai;
                TempData["AlertSuccess"] = "<script>document.getElementById('alertboxSucess').style.display = 'block'</script>";
                return View("Index");
            }
            if (idtontai && sTK.TinhTrang =="Close")
            {
                ViewBag.TinhTrang = sTK.TinhTrang;
                ViewBag.TienLai = sTK.TienLai;
                ViewBag.Message = "Thao tác thất bại, bạn không thể tất toán sổ đã đóng";
                TempData["AlertFail"] = "<script>document.getElementById('alertboxFail').style.display = 'block'</script>";
                return View("Index");
            }
            if (idtontai == false)
            {
                ViewBag.Message = "id không tồn tại";
                TempData["AlertFail"] = "<script>document.getElementById('alertboxFail').style.display = 'block'</script>";
                return View("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }
        public ActionResult Guithem(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STK sTK = db.STKs.Find(id);
            if (sTK == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            ViewBag.stkID = id;
            return View(sTK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Guithem([Bind(Include = "IdSTK,UserID,NganHang,NgayGui,SoTienGoc,KyHanGui,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] STK sTK, [Bind(Include = "sotiengui")]int soTiengui)
        {
            if (ModelState.IsValid)
            {

                sTK.SoTienGoc = sTK.SoTienGoc + soTiengui;
                db.Entry(sTK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }
        public ActionResult Rutmotphan(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STK sTK = db.STKs.Find(id);
            if (sTK == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            ViewBag.stkID = id;
            return View(sTK);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Rutmotphan([Bind(Include = "IdSTK,UserID,NganHang,NgayGui,SoTienGoc,KyHanGui,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] STK sTK,[Bind(Include ="sotienrut")]int soTienRut)
        {
            if (ModelState.IsValid)
            {
                sTK.SoTienGoc = sTK.SoTienGoc - soTienRut;
                db.Entry(sTK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", sTK.UserID);
            return View(sTK);
        }
    }
}
